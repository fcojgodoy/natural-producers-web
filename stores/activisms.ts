import { defineStore } from "pinia";
import type { IActivism } from "~/interfaces/activism";

export const useActivismsStore = defineStore("Activisms", () => {
  const apiBase = useRuntimeConfig().public.apiBase;

  const list = ref<IActivism[]>([]);

  const fetchList = async () => {
    try {
      const { data: activisms } = await useFetch<IActivism[]>(
        apiBase + "/activisms"
      );
      list.value = activisms.value || [];
    } catch (error) {
      console.log(error);
    }
  };

  const findById = (id: string): IActivism | undefined => {
    return list.value.find((activism) => activism._id === id);
  };

  const findBySlugEs = (slug: string): IActivism | undefined => {
    return list.value.find((activism) => activism.slug.es_es === slug);
  };

  const findMultipleByIds = (ids: string[]): IActivism[] => {
    return ids.map((id) => findById(id)).filter(Boolean) as IActivism[];
  };

  const findMultipleBySlugsEs = (slugs: string[]): IActivism[] => {
    return slugs
      .map((slug) => findBySlugEs(slug))
      .filter(Boolean) as IActivism[];
  };

  return { fetchList, findMultipleByIds, findMultipleBySlugsEs, list };
});
