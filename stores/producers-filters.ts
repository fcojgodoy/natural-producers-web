import { defineStore } from "pinia";
import type {
  IQueryParameters,
  IQueryParametersForMongoose,
} from "~/interfaces/producers-filters";

export const useProducersFiltersStore = defineStore("ProducersFilters", () => {
  const router = useRouter();

  const queryParameters = ref<IQueryParameters>({
    filters: {
      activisms: [],
      certifications: [],
      needs: [],
      producerTypes: [],
      productCategories: [],
      types: [],
    },
    pagination: {
      pageIndex: 1,
      pageSize: 10,
    },
    query: null,
    sort: {
      location: null,
    },
  });

  /**
   * Watch function to observe changes in the filters and update the URL accordingly.
   * This function converts non-empty filter values into a query parameters object,
   * updates the current URL parameters, and replaces the query string without reloading the page.
   *
   * @param {Object} newFilters - The new filter values.
   * @returns {void}
   */
  watch(
    () => queryParameters.value.filters,
    (newFilters) => {
      const queryParams: Record<string, string | null> = {
        activisms:
          newFilters.activisms.map((v) => v.slug.es_es).join(",") || null,
        certifications: newFilters.certifications.join(",") || null,
        needs: newFilters.needs.map((v) => v.slug.es_es).join(",") || null,
        producerTypes:
          newFilters.producerTypes.map((v) => v.slug.es_es).join(",") || null,
        productCategories:
          newFilters.productCategories.map((v) => v.slug.es_es).join(",") ||
          null,
      };

      const currentQueryParams = { ...router.currentRoute.value.query };

      Object.keys(queryParams).forEach((key) => {
        if (queryParams[key]) {
          // If the filter has a value, add or update it
          currentQueryParams[key] = queryParams[key];
        } else {
          // If the filter doesn't have a value, remove it from the current parameters
          delete currentQueryParams[key];
        }
      });

      // Replace the URL without reloading the page and replace the current query
      router.replace({
        query: {
          ...currentQueryParams, // Apply the updated query
        },
      });
    },
    { deep: true } // Necessary to observe changes within nested objects
  );

  /**
   * Building the filter in Mongoose style.
   * Reset pagination index to 1, too.
   *
   * @see https://mongoosejs.com/docs/api.html#model_Model.find
   */
  const queryParametersForMongoose = computed(
    (): IQueryParametersForMongoose => {
      const filters = queryParameters.value.filters;
      const sortLocation = queryParameters.value.sort.location;

      const query: IQueryParametersForMongoose = {};

      if (
        filters.activisms.length ||
        filters.needs.length ||
        filters.producerTypes.length ||
        filters.productCategories.length
      ) {
        // Reset pagination index
        queryParameters.value.pagination.pageIndex = 1;

        query.match = {};

        if (filters.activisms.length) {
          query.match.activisms = {
            $in: filters.activisms.map((v) => v._id),
          };
        }

        if (filters.needs.length) {
          query.match.needsCovered = {
            $in: filters.needs.map((v) => v._id),
          };
        }

        if (filters.producerTypes.length) {
          query.match.producerTypes = {
            $all: filters.producerTypes.map((v) => v._id),
          };
        }

        if (filters.productCategories.length) {
          query.match.productCategories = {
            $in: filters.productCategories.map((v) => v._id),
          };
        }
      }

      if (sortLocation?.geometry) {
        // Reset pagination index
        queryParameters.value.pagination.pageIndex = 1;

        query.geoNear = { location: sortLocation.geometry };
      }

      return query;
    }
  );

  const resetFilters = () => {
    queryParameters.value.filters.activisms = [];
    queryParameters.value.filters.certifications = [];
    queryParameters.value.filters.needs = [];
    queryParameters.value.filters.producerTypes = [];
    queryParameters.value.filters.productCategories = [];
    queryParameters.value.filters.types = [];
    queryParameters.value.query = null;
    queryParameters.value.sort.location = null;
    queryParameters.value.pagination.pageIndex = 1;
  };

  return { queryParameters, queryParametersForMongoose, resetFilters };
});
