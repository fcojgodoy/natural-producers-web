import { defineStore } from "pinia";
import type { ICertification } from "~/interfaces/certification";

export const useCertificationsStore = defineStore("Certifications", () => {
  const apiBase = useRuntimeConfig().public.apiBase;

  const list = ref<ICertification[]>([]);

  const fetchList = async () => {
    try {
      const { data: certifications } = await useFetch<ICertification[]>(
        apiBase + "/certifications",
      );
      list.value = certifications.value || [];
    } catch (error) {
      console.log(error);
    }
  };

  return { list, fetchList };
});
