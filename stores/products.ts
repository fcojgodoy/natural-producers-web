import { defineStore } from "pinia";
import type { IProduct } from "~/interfaces/product";

export const useProductsStore = defineStore("Products", () => {
  const apiBase = useRuntimeConfig().public.apiBase;

  const list = ref<IProduct[]>([]);

  const fetchList = async () => {
    try {
      const { data: products } = await useFetch<IProduct[]>(
        apiBase + "/products"
      );
      list.value = products.value || [];
    } catch (error) {
      console.log(error);
    }
  };

  const findById = (id: string): IProduct | undefined => {
    return list.value.find((activism) => activism._id === id);
  };

  const findBySlugEs = (slug: string): IProduct | undefined => {
    return list.value.find((product) => product.slug.es_es === slug);
  };
  const findMultipleByIds = (ids: string[]): IProduct[] => {
    return ids.map((id) => findById(id)).filter(Boolean) as IProduct[];
  };

  const findMultipleBySlugsEs = (slugs: string[]): IProduct[] => {
    return slugs
      .map((slug) => findBySlugEs(slug))
      .filter(Boolean) as IProduct[];
  };

  return { fetchList, findMultipleByIds, findMultipleBySlugsEs, list };
});
