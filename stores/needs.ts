import { defineStore } from "pinia";
import type { INeed } from "~/interfaces/need";

export const useNeedsStore = defineStore("Needs", () => {
  const apiBase = useRuntimeConfig().public.apiBase;

  const list = ref<INeed[]>([]);

  const fetchList = async () => {
    try {
      const { data: needs } = await useAsyncData<INeed[]>("needs", () =>
        $fetch(apiBase + "/needs")
      );
      list.value = needs.value || [];
    } catch (error) {
      console.log(error);
    }
  };

  const findBySlugEs = (slug: string): INeed | undefined => {
    return list.value.find((need) => need.slug.es_es === slug);
  };

  const findMultipleBySlugsEs = (slugs: string[]): INeed[] => {
    return slugs.map((slug) => findBySlugEs(slug)).filter(Boolean) as INeed[];
  };

  return { fetchList, findMultipleBySlugsEs, list };
});
