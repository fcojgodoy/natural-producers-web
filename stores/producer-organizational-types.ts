import { defineStore } from "pinia";
import type { IProducerOrganizationalType } from "~/interfaces/producer-organizational-types";

export const useProducerOrganizationalTypesStore = defineStore(
  "ProducerOrganizationalTypes",
  () => {
    const apiBase = useRuntimeConfig().public.apiBase;

    const list = ref<IProducerOrganizationalType[]>([]);

    const fetchList = async () => {
      try {
        const { data: producerOrganizationalTypes } = await useFetch<
          IProducerOrganizationalType[]
        >(`${apiBase}/organizational-types`);
        list.value = producerOrganizationalTypes.value || [];
      } catch (error) {
        console.error(error);
      }
    };

    return { list, fetchList };
  }
);
