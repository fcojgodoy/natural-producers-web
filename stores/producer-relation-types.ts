import { defineStore } from "pinia";
import type {
  IProducerRelationType,
  IProducerRelationTypeNames,
} from "~/interfaces/producer-relation-type";

export const useProducerRelationTypesStore = defineStore(
  "ProducerRelationTypes",
  () => {
    const apiBase = useRuntimeConfig().public.apiBase;

    const list = ref<IProducerRelationType[]>([]);

    const nameList = computed(() => {
      const result = {} as IProducerRelationTypeNames;
      list.value.forEach((relation) => {
        result[relation.code] = relation.name.es_es;
      });
      return <IProducerRelationTypeNames>result;
    });

    const fetchList = async () => {
      try {
        const { data: producerRelationTypes } = await useFetch<
          IProducerRelationType[]
        >(`${apiBase}/producer-relation-types`);
        list.value = producerRelationTypes.value || [];
      } catch (error) {
        console.error(error);
      }
    };

    return { fetchList, list, nameList };
  },
);
