import type { IPaginatedDocs } from "@/interfaces/paginated-docs";
import type { IProducer } from "@/interfaces/producer";
import { defineStore } from "pinia";
import { useRoute } from "vue-router";
import type { ILocation } from "~/interfaces/location";

export interface IProducerGeoMapped extends ILocation {
  producer: IProducer;
}

export const useProducersStore = defineStore("Producers", () => {
  const route = useRoute();
  const producersFiltersStore = useProducersFiltersStore();
  const apiBase = useRuntimeConfig().public.apiBase;

  // const emptyProducerDetail = {
  //   activisms: [],
  //   certifications: [],
  //   contactEmails: [],
  //   contactPhones: [],
  //   contactWebs: [],
  //   description: null,
  //   locations: [],
  //   name: null,
  //   needsCovered: [],
  //   organizationalType: null,
  //   productCategories: [],
  //   relations: {},
  //   slug: null,
  //   types: [],
  // };

  const emptyProducerPaginated: IPaginatedDocs<IProducer> = {
    docs: [],
    hasPrevPage: false,
    hasNextPage: false,
    limit: producersFiltersStore.queryParameters.pagination.pageSize,
    nextPage: null,
    page: producersFiltersStore.queryParameters.pagination.pageIndex,
    pagingCounter: 0,
    prevPage: null,
    totalDocs: 0,
    totalPages: 1,
  };

  const isLoadingList = ref<boolean>(true);
  const list = ref<IPaginatedDocs<IProducer>>(emptyProducerPaginated);
  const listGeoMapped = ref<IProducerGeoMapped[]>([]);
  const detail = ref<IProducer>();

  const fetchDetail = async () => {
    try {
      const { data: producer } = await useAsyncData<IProducer>("producer", () =>
        $fetch("/producers/slug/" + route.params.slug, { baseURL: apiBase })
      );
      producer.value && (detail.value = producer.value);
    } catch (error) {
      console.log(error);
    }
  };

  const fetchAllGeoMapped = async () => {
    try {
      isLoadingList.value = true;
      const { data } = await useFetch<IPaginatedDocs<IProducer>>("/producers", {
        query: {
          limit: 999999,
        },
        baseURL: apiBase,
      });
      listGeoMapped.value =
        data.value?.docs
          .filter((producer) => producer.locations.length)
          .map<IProducerGeoMapped>((producer) => {
            return (producer = {
              ...producer.locations[0],
              producer,
            });
          }) || [];
    } catch (error) {
      console.error(error);
    } finally {
      isLoadingList.value = false;
    }
  };

  const fetchList = async () => {
    try {
      isLoadingList.value = true;
      const { data } = await useFetch<IPaginatedDocs<IProducer>>("/producers", {
        query: {
          ...producersFiltersStore.queryParametersForMongoose,
          page: producersFiltersStore.queryParameters.pagination.pageIndex,
          limit: producersFiltersStore.queryParameters.pagination.pageSize,
        },
        baseURL: apiBase,
      });
      list.value = data.value || emptyProducerPaginated;
    } catch (error) {
      console.log(error);
    } finally {
      isLoadingList.value = false;
    }
  };

  return {
    detail,
    fetchAllGeoMapped,
    fetchDetail,
    fetchList,
    isLoadingList,
    list,
    listGeoMapped,
  };
});
