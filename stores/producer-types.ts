import { defineStore } from "pinia";
import type { IProducerType } from "~/interfaces/producer-type";

export const useProducerTypesStore = defineStore("ProducerTypes", () => {
  const apiBase = useRuntimeConfig().public.apiBase;

  const list = ref<IProducerType[]>([]);

  const fetchList = async () => {
    try {
      const { data: producerTypes } = await useFetch<IProducerType[]>(
        apiBase + "/producertypes"
      );
      list.value = producerTypes.value || [];
    } catch (error) {
      console.error(error);
    }
  };

  const findById = (id: string): IProducerType | undefined => {
    return list.value.find((producerType) => producerType._id === id);
  };

  const findBySlugEs = (slug: string): IProducerType | undefined => {
    return list.value.find((producerType) => producerType.slug.es_es === slug);
  };

  const findMultipleByIds = (ids: string[]): IProducerType[] => {
    return ids.map((id) => findById(id)).filter(Boolean) as IProducerType[];
  };

  const findMultipleBySlugsEs = (slugs: string[]): IProducerType[] => {
    return slugs
      .map((slug) => findBySlugEs(slug))
      .filter(Boolean) as IProducerType[];
  };

  return { fetchList, findMultipleByIds, findMultipleBySlugsEs, list };
});
