const runtimeConfig = useRuntimeConfig();

export default defineSitemapEventHandler(async () => {
  const producerSlugs = await $fetch<string[]>(
    runtimeConfig.public.apiBase + "/producers/slugs",
  );

  return producerSlugs.map((slug) =>
    asSitemapUrl({
      loc: `/productores/${slug}`,
    }),
  );
});
