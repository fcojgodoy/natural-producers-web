import { SITE_DESCRIPCION } from "~/constants/site-config";

export default {
  "@context": "https://schema.org",
  "@type": "Organization",
  name: "Natural Producers",
  description: SITE_DESCRIPCION,
  url: "https://naturalproducers.org",
  logo: "https://naturalproducers.org/favicon.png",
  contactPoint: {
    "@type": "ContactPoint",
    email: "hola@naturalproducers.org",
  },
};
