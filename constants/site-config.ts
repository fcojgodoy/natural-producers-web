export const SITE_DEFAULT_LOCALE = "es";
export const SITE_DESCRIPCION =
  "Descubre un directorio innovador que promueve la ecología y el consumo consciente. Conecta con productores y distribuidores locales que valoran la sostenibilidad y la comunidad. Ejerce tu consumo de manera responsable y apoya a la economía circular.";
export const SITE_NAME = "Natural Producers";
export const SITE_URL = "https://naturalproducers.org";
