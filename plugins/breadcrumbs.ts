import { defineNuxtPlugin } from "#app";

export default defineNuxtPlugin((nuxtApp) => {
  const { $registerBreadcrumbs } = nuxtApp;

  $registerBreadcrumbs([
    {
      name: "index",
      link: () => ({
        to: "/",
        title: "Ir a la página de inicio",
        text: "Inicio",
      }),
      children: [
        {
          name: "productores",
          link: () => ({
            to: "/productores",
            title: "Ir al listado de productores",
            text: "Productores",
          }),
          children: [
            {
              name: "productores-slug",
              link: (ctx: any) => ({
                to: `/productores/${ctx.slug}`,
                title: `Ir al detalle de ${ctx.producer.name}`,
                text: ctx.producer.name,
              }),
            },
          ],
        },
      ],
    },
  ]);
});
