import masonry from "vue-next-masonry";

export default defineNuxtPlugin((context) => {
  context.vueApp.use(masonry);
});
