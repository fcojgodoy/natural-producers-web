export interface ILocation {
  bbbox?: number[];
  geometry: {
    coordinates: [number, number];
  };
  properties: {
    address: {
      boundary: string;
      city: string;
      country: string;
      county: string;
      hamlet: string;
      house_number: string;
      industrial: string;
      place: string;
      postcode: string;
      road: string;
      state_district: string;
      state: string;
      town: string;
      village: string;
    };
    display_name: string;
  };
  streetAddressExtraInfo?: string;
}
