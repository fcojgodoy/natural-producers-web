import type { ILocalizedLiteral } from "./localized-literal";

export interface IActivism {
  _id: string;
  description: ILocalizedLiteral;
  icon: string;
  name: ILocalizedLiteral;
  slug: ILocalizedLiteral;
}
