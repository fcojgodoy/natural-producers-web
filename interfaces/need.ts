import type { ILocalizedLiteral } from "./localized-literal";

export interface INeed {
  _id: string;
  color: string;
  description: ILocalizedLiteral;
  icon: string;
  identifier: number;
  name: ILocalizedLiteral;
  slug: ILocalizedLiteral;
}
