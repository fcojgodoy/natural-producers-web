import type { IActivism } from "./activism";
import type { ILocation } from "./location";
import type { INeed } from "./need";
import type { IProducerType } from "./producer-type";
import type { IProduct } from "./product";

export interface IFilters {
  activisms: IActivism[];
  certifications: unknown[];
  needs: INeed[];
  producerTypes: IProducerType[];
  productCategories: IProduct[];
  types: unknown[];
}

export interface IPagination {
  pageIndex: number;
  pageSize: number;
}

export interface ISort {
  location: ILocation | null;
}

export interface IQueryParameters {
  filters: IFilters;
  pagination: IPagination;
  query: null;
  sort: ISort;
}

export interface IQueryParametersForMongoose {
  geoNear?: {
    location?: unknown;
  };
  match?: {
    activisms?: { $in: IActivism["_id"][] };
    needsCovered?: { $in: INeed["_id"][] };
    producerTypes?: { $all: IProducerType["_id"][] };
    productCategories?: { $in: IProduct["_id"][] };
    types?: unknown;
  };
}
