export interface CollaborateFormData {
  contact: string;
  description: string;
}
