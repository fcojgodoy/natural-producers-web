import type { IActivism } from "./activism";
import type { ICertification } from "./certification";
import type { ILocation } from "./location";
import type { INeed } from "./need";
import type { IProducerOrganizationalType } from "./producer-organizational-types";
import type { EProducerRelationTypeCode } from "./producer-relation-type";
import type { IProducerType } from "./producer-type";
import type { IProduct } from "./product";

export interface IProducer {
  _id: string;
  activisms: IActivism[];
  certifications: ICertification[];
  contactEmails: string[];
  contactPhones: string[];
  contactWebs: string[];
  description: string;
  locations: ILocation[];
  name: string;
  needsCovered: INeed[];
  organizationalType: IProducerOrganizationalType;
  ownerId: string;
  productCategories: IProduct[];
  relations: {
    [key in EProducerRelationTypeCode]?: IProducer[];
  };
  slug: string;
  status: number;
  types: IProducerType[];
}
