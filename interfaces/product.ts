import type { ILocalizedLiteral } from "./localized-literal";

export interface IProduct {
  __v: number;
  _id: string;
  alternateName: ILocalizedLiteral;
  descriptions: ILocalizedLiteral;
  keywords: ILocalizedLiteral;
  metaData: {
    iconCode: string;
    iconColor: string;
  };
  name: ILocalizedLiteral;
  slug: ILocalizedLiteral;
  type: string;
  typeId: number;
}
