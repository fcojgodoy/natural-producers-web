import type { ILocalizedLiteral } from "./localized-literal";

export interface IProducerOrganizationalType {
  _id: string;
  description: ILocalizedLiteral;
  name: ILocalizedLiteral;
}
