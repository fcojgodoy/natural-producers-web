import type { IActivism } from "./activism";
import type { ICertification } from "./certification";
import type { ILocation } from "./location";
import type { INeed } from "./need";
import type { IProducer } from "./producer";
import type { IProducerOrganizationalType } from "./producer-organizational-types";
import type { EProducerRelationTypeCode } from "./producer-relation-type";
import type { IProducerType } from "./producer-type";
import type { IProduct } from "./product";

export interface IProducerForm {
  activisms: IActivism["_id"][];
  certifications: ICertification["_id"][];
  contactEmails: string[];
  contactPhones: string[];
  contactWebs: string[];
  description: string;
  locations: ILocation[];
  name: string;
  needsCovered: INeed["_id"][];
  organizationalType: IProducerOrganizationalType["_id"] | null;
  relations: {
    [key in EProducerRelationTypeCode]?: IProducer["_id"][];
  };
  productCategories: IProduct["_id"][];
  types: IProducerType["_id"][];
}
