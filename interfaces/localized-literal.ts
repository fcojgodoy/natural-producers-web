export interface ILocalizedLiteral {
  en_us?: string;
  es_es: string;
}
