import type { ILocalizedLiteral } from "./localized-literal";

export interface ICertification {
  _id: string;
  abbreviation: string;
  name: ILocalizedLiteral;
  slug?: string;
}
