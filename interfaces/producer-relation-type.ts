import type { ILocalizedLiteral } from "./localized-literal";

export enum EProducerRelationTypeCode {
  Funder = "funder",
  FunderOf = "funderOf",
  Member = "member",
  MemberOf = "memberOf",
  ParentOrganization = "parentOrganization",
  Pos = "pos",
  PosOf = "posOf",
  Provider = "provider",
  ProviderOf = "providerOf",
  Sponsor = "sponsor",
  SponsorOf = "sponsorOf",
  SubOrganization = "subOrganization",
}

export interface IProducerRelationType {
  _id: string;
  code: EProducerRelationTypeCode;
  description: ILocalizedLiteral;
  name: ILocalizedLiteral;
  oppositeOf: IProducerRelationType["code"];
}

export type IProducerRelationTypeNames = {
  [value in EProducerRelationTypeCode]: string;
};
