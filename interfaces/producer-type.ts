import type { ILocalizedLiteral } from "./localized-literal";

export interface IProducerType {
  _id: string;
  code: string;
  description: ILocalizedLiteral;
  icon: string;
  name: ILocalizedLiteral;
  slug: ILocalizedLiteral;
}
