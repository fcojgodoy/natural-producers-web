export default defineNuxtRouteMiddleware((to) => {
  const path = to.path;
  if (path.length > 1 && path.endsWith("/")) {
    const newPath = path.slice(0, -1);
    return navigateTo(newPath, { redirectCode: 301 });
  }
});
