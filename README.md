# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```

### Regenerate favicon and app icons using RealFaviconGenerator

Install globally cli-real-favicon:

`npm install -g cli-real-favicon`

Edit `faviconDescription.json`.

The configuration can add or remove links or meta tags, which would require editing the `seo/favicon-meta.js` configuration.

The master picture for generate favicon and icons must be on `masterPicture` value on `faviconDescription.json`.

Generate the icons:

```bash
real-favicon generate faviconDescription.json faviconData.json public
```

More info in https://realfavicongenerator.net/

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
