import {
  SITE_DEFAULT_LOCALE,
  SITE_DESCRIPCION,
  SITE_NAME,
  SITE_URL,
} from "./constants/site-config";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    head: {
      script: [
        {
          hid: "goatcounter",
          "data-goatcounter": process.env.GOATCOUNTER_URL,
          async: true,
          src: "//gc.zgo.at/count.js",
        },
      ],
    },
  },
  auth: {
    baseURL: process.env.NUXT_PUBLIC_API_BASE
      ? process.env.NUXT_PUBLIC_API_BASE + "/users"
      : "http://localhost:4500/api/users",
    provider: {
      type: "local",
      endpoints: {
        signIn: { path: "/signin", method: "post" },
        signOut: { path: "/signout", method: "post" },
        signUp: { path: "/signup", method: "post" },
        getSession: { path: "/me", method: "get" },
      },
      pages: {
        login: "/login",
      },
      refresh: {
        isEnabled: true,
        endpoint: { path: "/token", method: "post" },
        refreshOnlyToken: false,
      },
    },
  },
  compatibilityDate: "2024-12-14",
  css: ["~/assets/icomoon/style.css", "~/assets/css/main.scss"],
  devtools: { enabled: true },
  modules: [
    "@pinia/nuxt",
    ["nuxt-jsonld", { disableOptionsAPI: true }],
    "@nuxtjs/sitemap",
    "@sidebase/nuxt-auth",
    "@vueuse/nuxt",
    "nuxt3-leaflet",
    "nuxt-breadcrumbs",
    "vuetify-nuxt-module",
  ],
  plugins: [{ src: "~/plugins/vue-next-masonry", ssr: false }],
  runtimeConfig: {
    public: {
      apiBase: "http://localhost:4500/api",
    },
  },
  site: {
    url: SITE_URL,
    name: SITE_NAME,
    description: SITE_DESCRIPCION,
    defaulLocale: SITE_DEFAULT_LOCALE,
  },
  sitemap: {
    sources: ["/api/__sitemap__/urls"],
  },
  vuetify: {
    vuetifyOptions: {
      theme: {
        themes: {
          light: {
            colors: {
              primary: "#6caa7f",
              secondary: "#325a57",
            },
          },
        },
      },
    },
  },
});
